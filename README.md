Install dependencies:

```
yarn install
```

Launch development server, which you can connect to via the Expo app:

```
yarn start 
```
