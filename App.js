import React from 'react';
import { 
  Alert,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
} from 'react-native';
import { Button } from 'react-native';


class Square extends React.Component {
  _pressed() {
    Alert.alert("I do not get it.");
  }
  render() {
    return (
      <TouchableHighlight underlayColor="white" onPress={this.props.chooseSquare}>
        <View style={{justifyContent: 'center', alignItems: 'center', width: 150, height: 150, backgroundColor: this.props.color }}>
          <Text style={styles.letter}>{this.props.letter}</Text>
        </View>
      </TouchableHighlight>
    )
  }
}

class Board extends React.Component {
  render() {
    let { board, chooseSquare } = this.props;
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <View style={{flexDirection: 'row'}}>
            <Square chooseSquare={() => {chooseSquare(7);}} letter={board[7]} color='powderblue' />
            <Square chooseSquare={() => {chooseSquare(8);}} letter={board[8]} color='skyblue' />
            <Square chooseSquare={() => {chooseSquare(9);}} letter={board[9]} color='steelblue' />
          </View>
          <View style={{flexDirection: 'row'}}>
            <Square chooseSquare={() => {chooseSquare(4);}} letter={board[4]} color='skyblue' />
            <Square chooseSquare={() => {chooseSquare(5);}} letter={board[5]} color='steelblue' />
            <Square chooseSquare={() => {chooseSquare(6);}} letter={board[6]} color='powderblue' />
          </View>
          <View style={{flexDirection: 'row'}}>
            <Square chooseSquare={() => {chooseSquare(1);}} letter={board[1]} color='steelblue' />
            <Square chooseSquare={() => {chooseSquare(2);}} letter={board[2]} color='powderblue' />
            <Square chooseSquare={() => {chooseSquare(3);}} letter={board[3]} color='skyblue' />
          </View>
          <View>
            {
              this.props.winner &&
              <Text style={styles.prompt}>Winner is {this.props.winner}!</Text>
            }
            {
              (!this.props.winner && this.props.message) &&
              <Text style={styles.prompt}>{this.props.message}</Text>
            }
          </View>
      </View>
    );
  }
}

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      board: [' ',
        ' ',' ',' ',
        ' ',' ',' ',
        ' ',' ',' ',
      ],
      turn: 'human',
      marker: 'X',
      winner: undefined,
      inPlay: true,
      message: 'Make your move',
    };
    this.state = Object.assign({}, this.initialState);
  }

  isWinner(bo, le) {
    // Given a board and a player's letter, this function returns True if that player has won.
    // We use bo instead of board and le instead of letter so we don't have to type as much.
    return (
      (bo[7] == le && bo[8] == le && bo[9] == le) ||  // across the top
      (bo[4] == le && bo[5] == le && bo[6] == le) ||  // across the middle
      (bo[1] == le && bo[2] == le && bo[3] == le) ||  // across the bottom
      (bo[7] == le && bo[4] == le && bo[1] == le) ||  // down the left side
      (bo[8] == le && bo[5] == le && bo[2] == le) ||  // down the middle
      (bo[9] == le && bo[6] == le && bo[3] == le) ||  // down the right side
      (bo[7] == le && bo[5] == le && bo[3] == le) ||  // diagonal
      (bo[9] == le && bo[5] == le && bo[1] == le)     // diagonal
    );
  }

  isSpaceFree(bo, square) {
    if(bo[square] === " ") {
      return true;
    }
    return false;
  }

  isBoardFull(bo) {
    let boardFull = true;
    for(let i = 1; i <= 9; i++) {
      if(this.isSpaceFree(bo, i)) {
        boardFull = false;
      }
    }
    return boardFull;
  }

  computerMove(board, piece) {
    // Here is our algorithm for our Tic Tac Toe AI:
    let move;
    let opPiece = 'X';
    if(piece === 'X') {
      opPiece = 'O';
    }

    console.log("Can I win in the next move?");
    for(let i=1; i<= 9; i++) {
      let b = board.slice();
      if (this.isSpaceFree(b, i)) {
        b[i] = piece;
        if(this.isWinner(b, piece)) {
          console.log("Yes.");
          return i;
        }
      }
    }

    console.log('Check if the player could win on their next move, should I block them?');
    for(let i=1; i<= 9; i++) {
      let b = board.slice();
      if (this.isSpaceFree(b, i)) {
        b[i] = opPiece;
        if(this.isWinner(b, opPiece)) {
          console.log("Yes.");
          return i;
        }
      }
    }

    console.log('Can I take one of the corners?');
    move = this.chooseRandomMoveFromList(board, [1,3,7,9]);
    if(move) {
      console.log("Yes.");
      return move;
    }

    console.log('Can I take the centre?');
    if(this.isSpaceFree(board, 5)) {
      console.log("Yes.");
      return 5;
    }

    console.log('Out of options, take one of the sides.');
    return this.chooseRandomMoveFromList(board, [2,4,6,8]);
  }

  chooseRandomMoveFromList(board, set) {
    let free = [];
    set.forEach( (i) => {
      if(this.isSpaceFree(board, i)) {
        free.push(i);
      }
    });
    if(free.length > 0) {
      return free[0];
    }
  }

  onSquare(square) {
    // Not play? Re-initialize the board
    if(!this.state.inPlay) {
      this.setState({...this.initialState});
      return;
    }
    // Make the move
    let board = this.state.board.slice();
    board[square] = this.state.marker;
    this.setState({
      board: board
    });

    if(this.isWinner(board, this.state.marker)) {
      // If there is a winner, set the state as such
      this.setState({
        winner: this.state.marker,
        inPlay: false,
      });
    } else {
      // Initialize for the next play
      let turn = "computer";
      let marker = "X";
      if(this.state.marker === 'X') {
        marker = 'O';
      }
      if(this.state.turn === 'computer') {
        turn = 'human';
      }
      this.setState({
        turn: turn,
        marker: marker,
      });
      if(turn === 'computer') {
        setTimeout(() => {
          console.log("My turn!");
          let move = this.computerMove(board, marker);
          if(move) {
            this.onSquare(move);
          }
        }, 1000);
      }
    }
    console.log(this.isBoardFull(board));
    // Is any further play possible?
    if(this.isBoardFull(board)) {
      this.setState({
        inPlay: false,
        message: "Stalemate",
      });
    }
  }

  render() {
    return (
      <Board 
        winner={this.state.winner}
        message={this.state.message}
        board={this.state.board}
        chooseSquare={this.onSquare.bind(this)}
      />
    );
  }
}

const styles = StyleSheet.create({
  prompt: {
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 56,
  },
  letter: {
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 100,
  },
  container: {
    flex: 3,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
